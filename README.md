# psycopg3-lambda-layer

AWS Lambda layer for psycopg3

To use in your serverless.yml:

```
functions:
  hello:
    handler: handler.hello
    layers:
      # py 3.9:
      - arn:aws:lambda:us-east-1:291551212413:layer:psycopg3-py39:2
```

## Regions

Please use the layer that matches your region, or you will get a permissions error.

If you desire another region, please open an issue.

## To build and deploy a new layer

Go to the directory of the python version that you want to build the layer for and then run `sls deploy`.

```
cd 3.9
yarn sls deploy
```

This will generate a new layer version and deploy it to the AWS. The output will be something like this:

```
layers:
  psycopg3-py39: arn:aws:lambda:us-east-1:291551212413:layer:psycopg3-py39:3
```

You can then use this layer version in your serverless.yml file.
